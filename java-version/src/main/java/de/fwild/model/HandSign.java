package de.fwild.model;

public enum HandSign {
    ROCK, PAPER, SCISSORS;

    /**
     * We compare the enum ordinals to retrieve what can beat what.
     * For neighboring signs the one with the higher ordinal wins.
     * For non-neighboring signs (diff > 1) we 'wrap' it around by shifting the result 3 positions.
     * @return True is only returned here if the diff is higher, that is: a sign doesn't beat itself.
     */
    public boolean beats(HandSign handSign) {
        int diff = this.ordinal() - handSign.ordinal();
        if (Math.abs(diff) > 1) {
            diff -= Math.signum(diff) * 3;
        }
        return diff > 0;
    }

}
