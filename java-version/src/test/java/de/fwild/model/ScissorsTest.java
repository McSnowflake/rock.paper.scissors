package de.fwild.model;

import org.junit.jupiter.api.Test;

import static de.fwild.model.HandSign.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ScissorsTest {

    @Test
    void vsPaperTest() {
        assertTrue(SCISSORS.beats(PAPER), "Scissors should not beat paper");
    }

    @Test
    void vsRockTest() {
        assertFalse(SCISSORS.beats(ROCK), "Scissors should not beat rock");
    }

    @Test
    void vsScissorsTest() {
        assertFalse(SCISSORS.beats(SCISSORS), "Scissors should not beat scissors");
    }
}
