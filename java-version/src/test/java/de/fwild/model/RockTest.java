package de.fwild.model;

import org.junit.jupiter.api.Test;

import static de.fwild.model.HandSign.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RockTest {

    @Test
    void vsPaperTest() {
        assertFalse(ROCK.beats(PAPER), "Rock should not beat paper");
    }

    @Test
    void vsRockTest() {
        assertFalse(ROCK.beats(ROCK), "Rock should not beat rock");
    }

    @Test
    void vsScissorsTest() {
        assertTrue(ROCK.beats(SCISSORS), "Rock should not beat scissors");
    }
}
