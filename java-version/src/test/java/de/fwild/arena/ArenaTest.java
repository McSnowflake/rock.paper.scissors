package de.fwild.arena;

import de.fwild.model.Strategy;
import de.fwild.players.PlayerFactory;
import de.fwild.players.PlayerInterface;
import de.fwild.ui.UserInterface;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static de.fwild.model.HandSign.PAPER;
import static de.fwild.model.HandSign.ROCK;
import static de.fwild.model.PlayerID.ONE;
import static de.fwild.model.PlayerID.TWO;
import static org.mockito.Mockito.*;

class ArenaTest {

    UserInterface testUI = mock(UserInterface.class);
    PlayerFactory testFactory = mock(PlayerFactory.class);
    PlayerInterface player1 = mock(PlayerInterface.class);
    PlayerInterface player2 = mock(PlayerInterface.class);


    @Test
    void addPlayersTest() {
        when(testUI.askForPlayerType(ONE, Strategy.getAll())).thenReturn(Strategy.CONSTANT_ROCK);
        when(testUI.askForPlayerType(TWO, Strategy.getAll())).thenReturn(Strategy.RANDOM_SIGN);

        when(testFactory.createPlayerWith(Strategy.CONSTANT_ROCK)).thenReturn(player1);
        when(testFactory.createPlayerWith(Strategy.RANDOM_SIGN)).thenReturn(player2);

        Arena sut = new Arena(testUI, testFactory);

        // actual test
        sut.readyPlayers();

        verify(testUI, times(1)).askForPlayerType(ONE, Strategy.getAll());
        verify(testUI, times(1)).askForPlayerType(TWO, Strategy.getAll());

        verify(testFactory, times(1)).createPlayerWith(Strategy.CONSTANT_ROCK);
        verify(testFactory, times(1)).createPlayerWith(Strategy.RANDOM_SIGN);

        verifyNoMoreInteractions(testUI, testFactory);
    }

    @Test
    void runMatch() {
        when(testUI.askForPlayerType(ONE, Strategy.getAll())).thenReturn(Strategy.CONSTANT_ROCK);
        when(testUI.askForPlayerType(TWO, Strategy.getAll())).thenReturn(Strategy.RANDOM_SIGN);

        when(testFactory.createPlayerWith(Strategy.CONSTANT_ROCK)).thenReturn(player1);
        when(testFactory.createPlayerWith(Strategy.RANDOM_SIGN)).thenReturn(player2);

        when(player1.nextSign()).thenReturn(ROCK);
        when(player2.nextSign()).thenReturn(PAPER);

        Arena sut = new Arena(testUI, testFactory);
        sut.readyPlayers();

        // actual test
        sut.startMatch();

        verify(testUI, times(1)).askForPlayerType(ONE, Strategy.getAll());
        verify(testUI, times(1)).askForPlayerType(TWO, Strategy.getAll());

        verify(testFactory, times(1)).createPlayerWith(Strategy.CONSTANT_ROCK);
        verify(testFactory, times(1)).createPlayerWith(Strategy.RANDOM_SIGN);

        verify(player1, times(4)).nextSign();
        verify(player2, times(4)).nextSign();

        verify(testUI, times(4)).publishFightResult(Map.of(ONE, ROCK, TWO, PAPER));
        verify(testUI, times(1)).publishGameResult(TWO, 1);
        verify(testUI, times(1)).publishGameResult(TWO, 2);
        verify(testUI, times(1)).publishMatchResult(TWO, 2);

        verifyNoMoreInteractions(testFactory, testUI, player1, player2);
    }
}
