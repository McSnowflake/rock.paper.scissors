package de.fwild.model

enum class PlayerID {
    ONE, TWO
}