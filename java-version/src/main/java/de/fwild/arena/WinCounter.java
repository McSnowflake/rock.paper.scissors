package de.fwild.arena;

import de.fwild.model.PlayerID;

import static de.fwild.model.PlayerID.ONE;
import static de.fwild.model.PlayerID.TWO;

public class WinCounter {
    private int playerOneWins = 0;
    private int playerTwoWins = 0;

    public void update(PlayerID winner) {
        switch (winner) {
            case ONE:
                playerOneWins++;
                break;
            case TWO:
                playerTwoWins++;
                break;
            default:
                throw new RuntimeException("Three player game not supported");
        }
    }

    public boolean isDraw() {
        return playerOneWins == playerTwoWins;
    }

    public boolean hasNoWinner() {
        return !(playerOneWins >= 2 || playerTwoWins >= 2) || isDraw();
    }

    public PlayerID getWinner() {
        if (playerOneWins > playerTwoWins) return ONE;
        else if (playerOneWins < playerTwoWins) return TWO;
        else throw new RuntimeException("Winner requested during DRAW");
    }

}
