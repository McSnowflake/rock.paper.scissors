package de.fwild.ui;

import de.fwild.model.HandSign;
import de.fwild.model.PlayerID;
import de.fwild.model.Strategy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

@SuppressWarnings("PMD.SystemPrintln")
public class ConsoleUserInterface implements UserInterface {

    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public Strategy askForPlayerType(PlayerID player, Map<String, String> availableStrategies) {
        String answer = null;

        while (answer == null) {
            System.out.println("What kind of strategy should player " + player + " follow?");
            availableStrategies.forEach((number, strategy) -> System.out.println("(" + number + ") " + strategy));
            try {
                answer = reader.readLine();
            } catch (IOException e) {
                throw new RuntimeException("Console Interaction failed", e);
            }
            if (!availableStrategies.containsKey(answer)) {
                System.out.println("Please select one of the provided strategies by entering the according number");
                answer = null;
            }
        }

        return Strategy.valueOf(availableStrategies.get(answer));
    }

    @Override
    public boolean askToContinue() {
        System.out.println("Do you want to run another game?");
        String answer;
        try {
            answer = reader.readLine();
        } catch (IOException e) {
            throw new RuntimeException("Console Interaction failed", e);
        }
        return "y".equals(answer.toLowerCase(Locale.ROOT));
    }

    @Override
    public void publishFightResult(final Map<PlayerID, HandSign> roundResult) {
        String message = roundResult.entrySet().stream()
                .map(result -> "Player " + result.getKey() + ": " + result.getValue())
                .collect(Collectors.joining(" - "));
        System.out.println(message);
    }

    @Override
    public void publishGameResult(final PlayerID winner, final int gameNumber) {
        System.out.println("Player " + winner + " has won game number " + gameNumber);
        System.out.println();
    }

    @Override
    public void publishMatchResult(final PlayerID winner, final int numberOfGames) {
        System.out.println("Game over! Player " + winner + " has won after " + numberOfGames + " games.");
        System.out.println();
    }
}
