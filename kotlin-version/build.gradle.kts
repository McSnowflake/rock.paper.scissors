plugins {
    kotlin("jvm") version "1.6.10"
}

group = "de.fwild"
version = "0.1.0"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))
}