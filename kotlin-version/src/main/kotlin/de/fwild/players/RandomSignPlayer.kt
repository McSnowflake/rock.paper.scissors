package de.fwild.players

import de.fwild.model.HandSign
import java.util.*

class RandomSignPlayer : PlayerInterface {

    var rand = Random()
    var elements: Int = HandSign.values().size

    override fun nextSign() = HandSign.values()[rand.nextInt(elements)]

}