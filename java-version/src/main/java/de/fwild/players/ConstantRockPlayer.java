package de.fwild.players;

import de.fwild.model.HandSign;

public class ConstantRockPlayer implements PlayerInterface {

    @Override
    public HandSign nextSign() {
        return HandSign.ROCK;
    }
}
