package de.fwild.arena

import de.fwild.model.HandSign
import de.fwild.model.PlayerID
import de.fwild.model.Strategy
import de.fwild.players.PlayerFactory
import de.fwild.players.PlayerInterface
import de.fwild.ui.UserInterface

/**
 * The arena controls the game flow and keeps track of the statistics.
 *
 *
 * It requires a user interface so that it can be interacted with.
 */
class Arena(
    private val userInterface: UserInterface,
    private val factory: PlayerFactory
) {
    private val players: MutableMap<PlayerID, PlayerInterface> = mutableMapOf()

    /**
     * This method will request a strategy for each player and create the according players.
     */
    fun readyPlayers() {
        PlayerID.values().forEach { createPlayer(it) }
    }

    /**
     * This method will start games with the current players until a match winner is decided.
     */
    fun startMatch() = with(WinCounter()) {
        var round = 0
        while (hasNoWinner()) {
            round++
            val winner = startGame()
            recordAsWinner(winner)
            userInterface.publishGameResult(winner, round)
        }
        userInterface.publishMatchResult(getWinner(), round)
    }


    /**
     * This method will make the current players fight until one wins the current game.
     */
    private fun startGame(): PlayerID = with(WinCounter()) {
        while (hasNoWinner()) {
            val result = fight()
            when {
                result[PlayerID.ONE]!!.beats(result[PlayerID.TWO]!!) -> recordAsWinner(PlayerID.ONE)
                result[PlayerID.TWO]!!.beats(result[PlayerID.ONE]!!) -> recordAsWinner(PlayerID.TWO)
            }
            userInterface.publishFightResult(result)
        }
        return getWinner()
    }

    /**
     * @return The signs provided by every player
     */
    private fun fight(): Map<PlayerID, HandSign> = players.entries.associate { Pair(it.key, it.value.nextSign()) }

    private fun createPlayer(playerID: PlayerID) {
        val selectedStrategy = userInterface.askForPlayerType(playerID, Strategy.getAll())
        val player: PlayerInterface = factory.createPlayerWith(selectedStrategy)
        players[playerID] = player
    }
}