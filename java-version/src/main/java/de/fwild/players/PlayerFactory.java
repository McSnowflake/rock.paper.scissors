package de.fwild.players;

import de.fwild.model.Strategy;

public class PlayerFactory {
    public PlayerInterface createPlayerWith(Strategy strategy) {
        switch (strategy) {
            case RANDOM_SIGN:
                return new RandomSignPlayer();
            case CONSTANT_ROCK:
                return new ConstantRockPlayer();
            default:
                throw new RuntimeException("Strategy not implemented yet: " + strategy.name());
        }
    }
}
