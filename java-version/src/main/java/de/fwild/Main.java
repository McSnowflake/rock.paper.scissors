package de.fwild;

import de.fwild.arena.Arena;
import de.fwild.players.PlayerFactory;
import de.fwild.ui.ConsoleUserInterface;
import de.fwild.ui.UserInterface;

public class Main {

    public static void main(String[] args) {

        UserInterface userInterface = new ConsoleUserInterface();
        PlayerFactory factory = new PlayerFactory();
        Arena arena = new Arena(userInterface, factory);

        do {
            arena.readyPlayers();
            arena.startMatch();
        } while (userInterface.askToContinue());

    }
}
