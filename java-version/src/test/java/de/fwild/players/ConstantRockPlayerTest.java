package de.fwild.players;

import de.fwild.model.HandSign;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertSame;

class ConstantRockPlayerTest {

    @Test
    void alwaysReturnsRock() {

        PlayerInterface sut = new ConstantRockPlayer();

        for (int i = 0; i < 100; i++) {
            HandSign result = sut.nextSign();

            assertSame(HandSign.ROCK, result, "ConstantRock player should always return rock");
        }
    }
}
