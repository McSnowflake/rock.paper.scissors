package de.fwild.players;

import de.fwild.model.HandSign;

import java.util.Random;

public class RandomSignPlayer implements PlayerInterface {

    Random rand = new Random();
    Integer elements = HandSign.values().length;

    @Override
    public HandSign nextSign() {
        return HandSign.values()[rand.nextInt(elements)];
    }
}
