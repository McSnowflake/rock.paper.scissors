package de.fwild.players

import de.fwild.model.Strategy
import de.fwild.model.Strategy.CONSTANT_ROCK
import de.fwild.model.Strategy.RANDOM_SIGN

class PlayerFactory {
    fun createPlayerWith(strategy: Strategy) = when (strategy) {
        RANDOM_SIGN -> RandomSignPlayer()
        CONSTANT_ROCK -> ConstantRockPlayer()
    }
}
