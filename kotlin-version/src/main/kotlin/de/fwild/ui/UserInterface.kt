package de.fwild.ui

import de.fwild.model.HandSign
import de.fwild.model.PlayerID
import de.fwild.model.Strategy

interface UserInterface {
    fun askForPlayerType(player: PlayerID, availableStrategies: Map<String, String>): Strategy
    fun askToContinue(): Boolean
    fun publishFightResult(result: Map<PlayerID, HandSign>)
    fun publishGameResult(winner: PlayerID, roundNumber: Int)
    fun publishMatchResult(winner: PlayerID, numberOfGames: Int)
}