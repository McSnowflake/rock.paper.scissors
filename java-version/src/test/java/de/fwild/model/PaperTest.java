package de.fwild.model;

import org.junit.jupiter.api.Test;

import static de.fwild.model.HandSign.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PaperTest {
    @Test
    void vsPaperTest() {
        assertFalse(PAPER.beats(PAPER), "Paper should not beat paper");
    }

    @Test
    void vsRockTest() {
        assertTrue(PAPER.beats(ROCK), "Paper should beat rock");
    }

    @Test
    void vsScissorsTest() {
        assertFalse(PAPER.beats(SCISSORS), "Paper should not beat scissors");
    }
}
