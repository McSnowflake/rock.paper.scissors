package de.fwild.players;

import de.fwild.model.HandSign;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RandomPlayerTest {

    @Test
    void returnsRandomElements() {

        PlayerInterface sut = new ConstantRockPlayer();

        for (int i = 0; i < 100; i++) {
            HandSign result = sut.nextSign();

            assertNotNull(result, "ConstantRock player should always return rock");
        }
    }
}
