plugins {
    java
    pmd
}

group = "de.fwild"
version = "0.1.0"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.1")
    testImplementation("org.junit.jupiter:junit-jupiter-params:5.8.1")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
    testImplementation("org.mockito:mockito-core:3.+")
}


pmd {
    isConsoleOutput = true
    toolVersion = "6.44.0"
    rulesMinimumPriority.set(4)
    ruleSets = listOf("category/java/bestpractices.xml")
}


tasks.getByName<Test>("test") {
    useJUnitPlatform()
}