package de.fwild.model;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public enum Strategy {
    RANDOM_SIGN, CONSTANT_ROCK;

    public static Map<String, String> getAll() {
        return Arrays.stream(Strategy.values()).collect(Collectors.toMap(v ->  String.valueOf(v.ordinal()), Enum::name));
    }

}
