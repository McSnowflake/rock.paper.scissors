package de.fwild.model

import kotlin.math.abs
import kotlin.math.sign

enum class HandSign {
    ROCK, PAPER, SCISSORS;

    /**
     * We compare the enum ordinals to retrieve what can beat what.
     * For neighboring signs the one with the higher ordinal wins.
     * For non-neighboring signs (diff > 1) we 'wrap' it around by shifting the result 3 positions.
     * @return True is only returned here if the diff is higher, that is: a sign doesn't beat itself.
     */
    fun beats(handSign: HandSign): Boolean {
        var diff = ordinal - handSign.ordinal
        if (abs(diff) > 1) {
            diff -= (sign(diff.toFloat()) * 3).toInt()
        }
        return diff > 0
    }
}