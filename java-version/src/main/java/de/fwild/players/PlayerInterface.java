package de.fwild.players;

import de.fwild.model.HandSign;

public interface PlayerInterface {
    HandSign nextSign();
}
