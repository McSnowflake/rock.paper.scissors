# Rock.Paper.Scissors

A simple rock, paper, scissors simulator.

Hint: there are two modules in this project, because it has (/will have) two implementations: one in Java and one in
Kotlin. (although the kotlin implementation has not started yet)

## Domain Syntax
Code naming conventions relate to the following game rules:

*A Game: The first player with two wins, wins a game.*

*A Match: The first player to win two games, wins the match.*

## Architecture description

The core of the simulator is the Arena, where all the game flow logic resides.

The arena takes a user interface as input, which capsules all user interaction. Currently, there is only one
implementation, which enables interaction via the console.

For each game that takes place in the arena, two new players will be generated (by the users choice). Currently, the
PlayerInterface is implemented by two "strategies": always returning rock and returning a random sign.

## QA

Switch into the folder of the module you want to test and run `./gradlew test` or `./gradlew lint`
