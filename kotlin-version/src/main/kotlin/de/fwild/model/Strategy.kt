package de.fwild.model

enum class Strategy {
    RANDOM_SIGN, CONSTANT_ROCK;

    companion object {
        fun getAll() = values().associate { Pair(it.ordinal.toString(), it.name) }
    }
}