package de.fwild

import de.fwild.arena.Arena
import de.fwild.players.PlayerFactory
import de.fwild.ui.ConsoleUserInterface


fun main() {

    val userInterface = ConsoleUserInterface()
    val factory = PlayerFactory()
    val arena = Arena(userInterface, factory)

    do {
        arena.readyPlayers()
        arena.startMatch()
    } while (userInterface.askToContinue())

}