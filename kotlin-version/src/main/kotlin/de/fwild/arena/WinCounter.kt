package de.fwild.arena

import de.fwild.model.PlayerID
import de.fwild.model.PlayerID.ONE
import de.fwild.model.PlayerID.TWO

class WinCounter {

    private var playerOneWins = 0
    private var playerTwoWins = 0

    fun recordAsWinner(winner: PlayerID) {
        when (winner) {
            ONE -> playerOneWins++
            TWO -> playerTwoWins++
        }
    }

    fun hasNoWinner() = !(playerOneWins >= 2 || playerTwoWins >= 2) || playerOneWins == playerTwoWins

    fun getWinner() = when {
        playerOneWins > playerTwoWins -> ONE
        playerOneWins < playerTwoWins -> TWO
        else -> throw Exception("Winner requested during DRAW")
    }
}