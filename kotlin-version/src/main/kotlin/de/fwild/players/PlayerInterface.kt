package de.fwild.players

import de.fwild.model.HandSign

interface PlayerInterface {
    fun nextSign(): HandSign
}