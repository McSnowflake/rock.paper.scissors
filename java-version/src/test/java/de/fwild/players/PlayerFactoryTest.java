package de.fwild.players;

import de.fwild.model.Strategy;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import static org.junit.jupiter.api.Assertions.assertTrue;

class PlayerFactoryTest {

    private final PlayerFactory factory = new PlayerFactory();

    @ParameterizedTest
    @EnumSource(Strategy.class)
    @SuppressWarnings("PMD.JUnitTestContainsTooManyAsserts")
    void createsPlayerAccordingToStrategy(Strategy strategy) throws Exception {

        PlayerInterface createdPlayer = factory.createPlayerWith(strategy);
        switch (strategy) {
            case RANDOM_SIGN:
                assertTrue(createdPlayer instanceof RandomSignPlayer,
                        "The player created for RandomSign strategy should be of type RandomSignPlayer");
                break;
            case CONSTANT_ROCK:
                assertTrue(createdPlayer instanceof ConstantRockPlayer,
                        "The player created for ConstantStone strategy should be of type ConstantRockPlayer");
                break;
            default:
                throw new Exception("Untested Strategy: " + strategy);
        }
    }
}
