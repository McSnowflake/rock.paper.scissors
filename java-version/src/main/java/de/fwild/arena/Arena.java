package de.fwild.arena;

import de.fwild.model.HandSign;
import de.fwild.model.PlayerID;
import de.fwild.model.Strategy;
import de.fwild.players.PlayerFactory;
import de.fwild.players.PlayerInterface;
import de.fwild.ui.UserInterface;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static de.fwild.model.PlayerID.ONE;

/**
 * The arena controls the game flow and keeps track of the statistics.
 * <p>
 * It requires a user interface so that it can be interacted with.
 */
public class Arena {

    private final Map<PlayerID, PlayerInterface> players = new HashMap<>();

    private final UserInterface userInterface;
    private final PlayerFactory factory;

    public Arena(final UserInterface userInterface, PlayerFactory factory) {
        this.userInterface = userInterface;
        this.factory = factory;
    }

    /**
     * This method will request a strategy for each player and create the according players.
     */
    public void readyPlayers() {
        Arrays.stream(PlayerID.values()).forEach(this::createPlayer);
    }

    /**
     * This method will start games with the current players until a match winner is decided.
     */
    public void startMatch() {
        WinCounter matchStats = new WinCounter();
        int round = 0;

        while (matchStats.hasNoWinner()) {
            round++;
            PlayerID winner = startGame();
            matchStats.update(winner);
            userInterface.publishGameResult(winner, round);
        }

        userInterface.publishMatchResult(matchStats.getWinner(), round);
    }

    /**
     * This method will make the current players fight until one wins the current game.
     */
    private PlayerID startGame() {
        WinCounter gameStats = new WinCounter();
        while (gameStats.hasNoWinner()) {

            Map<PlayerID, HandSign> result = fight();

            if (!result.get(ONE).equals(result.get(PlayerID.TWO))) {
                if (result.get(ONE).beats(result.get(PlayerID.TWO))) {
                    gameStats.update(PlayerID.ONE);
                } else {
                    gameStats.update(PlayerID.TWO);
                }
            }

            userInterface.publishFightResult(result);

        }

        return gameStats.getWinner();
    }

    /**
     * @return The signs provided by every player
     */
    private Map<PlayerID, HandSign> fight() {
        return players
                .entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().nextSign()));
    }

    private void createPlayer(PlayerID playerID) {
        Strategy selectedStrategy = userInterface.askForPlayerType(playerID, Strategy.getAll());
        PlayerInterface player =  factory.createPlayerWith(selectedStrategy);
        players.put(playerID, player);
    }
}
