package de.fwild.players

import de.fwild.model.HandSign

class ConstantRockPlayer : PlayerInterface {
    override fun nextSign() = HandSign.ROCK
}