package de.fwild.ui;

import de.fwild.model.HandSign;
import de.fwild.model.PlayerID;
import de.fwild.model.Strategy;

import java.util.Map;

public interface UserInterface {

    Strategy askForPlayerType(PlayerID player, Map<String, String> availableStrategies);

    boolean askToContinue();

    void publishFightResult(Map<PlayerID, HandSign> result);

    void publishGameResult(PlayerID winner, int roundNumber);

    void publishMatchResult(PlayerID winner, int round);
}
