package de.fwild.ui

import de.fwild.model.HandSign
import de.fwild.model.PlayerID
import de.fwild.model.Strategy
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader

class ConsoleUserInterface : UserInterface {
    var reader: BufferedReader = BufferedReader(InputStreamReader(System.`in`))
    override fun askForPlayerType(player: PlayerID, availableStrategies: Map<String, String>): Strategy {

        while (true) {
            println("What kind of strategy should player $player follow?")
            availableStrategies.forEach { (number: String, strategy: String) -> println("($number) $strategy") }
            val answer = try {
                reader.readLine()
            } catch (e: IOException) {
                throw RuntimeException("Console Interaction failed", e)
            }
            if (!availableStrategies.containsKey(answer)) {
                println("Please select one of the provided strategies by entering the according number")
            } else
                return Strategy.valueOf(availableStrategies[answer]!!)
        }

    }

    override fun askToContinue(): Boolean {
        println("Do you want to run another game?")
        val answer: String = try {
            reader.readLine()
        } catch (e: IOException) {
            throw RuntimeException("Console Interaction failed", e)
        }
        return "y" == answer.lowercase()
    }

    override fun publishFightResult(result: Map<PlayerID, HandSign>) {
        println(result.entries.joinToString("   -   ") { "Player ${it.key}: ${it.value}" })
    }

    override fun publishGameResult(winner: PlayerID, roundNumber: Int) {
        println("Player $winner has won game number $roundNumber")
        println()
    }

    override fun publishMatchResult(winner: PlayerID, numberOfGames: Int) {
        println("Game over! Player $winner has won after $numberOfGames games.")
        println()
    }
}